var bgColor; // sets color of background
var xStart; // starting point x axis
var yStart;  // starting point y axis
var diamX;  // x axis diameter of ellipse
var diamY;  // y axis diameter of ellipse
var xScale;
var yScale;
var sColor;  // stroke color
 
function setup(){
  createCanvas(400,400);
  smooth();
  xStart = 200;
  yStart = 200;
  diamX = 120;
  diamY = 20;
  xScale = 2;
  yScale = 2;
  sColor = 255;
  bgColor = 0;
  
  
}
 
 
function draw(){
  background(bgColor);
  stroke(sColor);
  for(var i = 0; i < 20; i ++){
    push();
     
    noFill();
    translate(xStart,yStart);
    scale(xScale,yScale);
    rotate(radians(i*mouseY));
    ellipse(0,0,diamX,diamY);
    pop();
     
  }
}