// Initialize Firebase
var config = {
  apiKey: "AIzaSyDy6MrF14_dfbxeJ_-ckzHAPbbb3rIu93Q",
  authDomain: "itp16-drawing-p5.firebaseapp.com",
  databaseURL: "https://itp16-drawing-p5.firebaseio.com",
  storageBucket: "itp16-drawing-p5.appspot.com",
};
firebase.initializeApp(config);
var db =  firebase.database();

function setup() {
  createCanvas(800, 600);
  strokeWeight(5);
}

function draw() {
  
}

function mouseDragged(){

  db.ref('drawing').push({
    px: pmouseX, 
    py: pmouseY, 
    x: mouseX, 
    y: mouseY
  });
  
  db.ref('drawing').on('child_added', function(snapshot){
    var data = snapshot.val();
    line(data.px, data.py, data.x, data.y);
    println(data);
  });
  
  //line(pmouseX, pmouseY, mouseX, mouseY);
}